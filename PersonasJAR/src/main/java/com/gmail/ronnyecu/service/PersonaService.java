package com.gmail.ronnyecu.service;

import com.gmail.ronnyecu.constants.Constants;
import com.gmail.ronnyecu.model.Persona;
import com.gmail.ronnyecu.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonaService {

    public List<Persona> personaList() {
        return Arrays.asList(new Gson().fromJson(Utils.readUrl(Constants.URL_PERSONAS_JSON), Persona[].class));
    }
}
