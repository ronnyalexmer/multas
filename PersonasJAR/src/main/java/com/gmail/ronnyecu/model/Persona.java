package com.gmail.ronnyecu.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Persona {

    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("registered")
    @Expose
    private String registered;
    @SerializedName("eyeColor")
    @Expose
    private String eyeColor;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("friends")
    @Expose
    private List<Friend> friends = null;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("greeting")
    @Expose
    private String greeting;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("_id")
    @Expose
    private String id;

    @Override
    public String toString() {
        return "Persona{" +
                ", latitude=" + latitude +
                ", company='" + company + '\'' +
                ", email='" + email + '\'' +
                ", picture='" + picture + '\'' +
                ", tags=" + tags +
                ", registered='" + registered + '\'' +
                ", eyeColor='" + eyeColor + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", friends=" + friends +
                ", isActive=" + isActive +
                ", about='" + about + '\'' +
                ", balance='" + balance + '\'' +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", greeting='" + greeting + '\'' +
                ", longitude=" + longitude +
                ", id='" + id + '\'' +
                '}';
    }
}