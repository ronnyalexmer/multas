<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Multas!</h1>
        <table border="1">
            <thead>
            <tr>
                <th>numero</th>
                <th>Fecha</th>
                <th>Lugar</th>
                <th>Persona</th>
                <th>Importe</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="multa" items="${multas}">
                <tr>
                    <td>${multa.numero}</td>
                    <td>
                        <fmt:formatDate value="${multa.fecha}" pattern="dd-MMM-yyyy"/>
                    </td>
                    <td>${multa.lugar}</td>
                    <td>${multa.persona.name}</td>
                    <td>${multa.importe}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>


    </body>
</html>
