package com.gmail.ronnyecu.model;


import com.gmail.ronnyecu.model.beans.Vehiculo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Multa {
    @SerializedName("numero")
    @Expose
    private int numero;
    @SerializedName("fecha")
    @Expose
    private Date fecha;
    @SerializedName("lugar")
    @Expose
    private String lugar;
    @SerializedName("importe")
    @Expose
    private double importe;
    @SerializedName("persona")
    @Expose
    private Persona persona;
    @SerializedName("vehiculo")
    @Expose
    private Vehiculo vehiculo;

    public Multa(int numero, Date fecha, String lugar, double importe, Persona persona) {
        this.numero = numero;
        this.fecha = fecha;
        this.lugar = lugar;
        this.importe = importe;
        this.persona = persona;
    }

    @Override
    public String toString() {
        return "Multa{" +
                "numero=" + numero +
                ", fecha='" + fecha + '\'' +
                ", lugar='" + lugar + '\'' +
                ", importe=" + importe +
                '}';
    }
}
