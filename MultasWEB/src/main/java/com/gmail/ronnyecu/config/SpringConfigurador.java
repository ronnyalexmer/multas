package com.gmail.ronnyecu.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.gmail.ronnyecu")
public class SpringConfigurador {
}