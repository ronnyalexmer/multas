package com.gmail.ronnyecu.controller;

import com.gmail.ronnyecu.model.Multa;
import com.gmail.ronnyecu.service.MultaService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/multas", method = RequestMethod.GET)
public class MultasController {
    static Log _log = LogFactory.getLog(MultasController.class.getSimpleName());

    @RequestMapping(value = "/showAll", method = RequestMethod.GET)
    public List<Multa> getMultas() {
        return new MultaService().getAllMultas();
    }

    @RequestMapping(value = "/date", method = RequestMethod.GET)
    public List<Multa> getMultaByDate(@NotNull @RequestParam("after") String after, @NotNull @RequestParam("before") String before) {
        List<Multa> multas = new ArrayList<>();
        try {
            Date afterDate = new SimpleDateFormat("dd/MM/yyyy").parse(after);
            Date beforeDate = new SimpleDateFormat("dd/MM/yyyy").parse(before);

            multas.addAll(new MultaService().getAllMultas().stream().filter(m -> m.getFecha().after(afterDate) && m.getFecha().before(beforeDate)).collect(Collectors.toList()));
        } catch (java.text.ParseException ex) {
            _log.error(String.format("Error de formato en la before: %s \t after: %s", before, after), ex);
        }
        return multas;
    }

}
