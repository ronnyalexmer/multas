package com.gmail.ronnyecu.service;

import com.gmail.ronnyecu.model.Multa;
import com.gmail.ronnyecu.model.Persona;
import com.gmail.ronnyecu.model.vehiculos.Ciclomotor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class MultaService {
    public List<Multa> getAllMultas() {
        Random rand = new Random();
        List<Multa> multas = new ArrayList<Multa>();
        List<Ciclomotor> ciclomotores = new VehiculoService().getCicloMotores();
        final List<Persona> personas = new PersonaService().personaList();

        LocalDateTime localDateTime = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        personas.forEach(persona ->
                multas.add(new Multa(
                                multas.size(),
                                Date.from(localDateTime.plusDays(multas.size()).atZone(ZoneId.systemDefault()).toInstant()),
                                "España",
                                30.50,
                                persona
                        )
                ));

        multas.forEach(multa -> {
                    int i = rand.nextInt(ciclomotores.size());
                    multa.setVehiculo(ciclomotores.get(i));
                    //  ciclomotores.remove(i);
                }
        );

        return multas;
    }

}
