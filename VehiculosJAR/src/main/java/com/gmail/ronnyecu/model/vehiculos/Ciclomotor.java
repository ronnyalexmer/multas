package com.gmail.ronnyecu.model.vehiculos;

import com.gmail.ronnyecu.model.beans.Vehiculo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Ciclomotor extends Vehiculo {
    @SerializedName("cc")
    @Expose
    private String cc;


    public Ciclomotor(String modelo, String cc) {
        super(modelo);
        this.cc = cc;
    }

    @Override
    public String getMarca() {
        return super.getModelo().split(" ")[0].trim();
    }

    @Override
    public String getModelo() {
        return super.getModelo().substring(getMarca().length()).trim();
    }


    @Override
    public String toString() {
        return "Ciclomotor{" +
                "cc=" + cc +
                ", marca='" + this.getMarca() + '\'' +
                ", modelo='" + this.getModelo() + '\'' +
                '}';
    }
}
