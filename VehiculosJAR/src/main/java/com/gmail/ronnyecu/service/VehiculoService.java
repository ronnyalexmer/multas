package com.gmail.ronnyecu.service;

import com.gmail.ronnyecu.constants.Constants;
import com.gmail.ronnyecu.model.vehiculos.Ciclomotor;
import com.gmail.ronnyecu.utils.Utils;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class VehiculoService {
    public List<Ciclomotor> getCicloMotores() {
        return Arrays.asList(new Gson()
                .fromJson(Utils.getVehiculosFromUrl(Constants.JSON_VEHICULO_EXPRESSION, Constants.URL_CICLOMOTOR_JSON).toString(),
                        Ciclomotor[].class));
    }
}
