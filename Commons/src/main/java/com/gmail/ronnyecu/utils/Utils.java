package com.gmail.ronnyecu.utils;

import com.gmail.ronnyecu.constants.Constants;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Utils {
    public static String readUrl(String urlString) {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }

    public static JSONArray getVehiculosFromUrl(String expression, String url) {
        JSONObject tmp = new JSONObject(readUrl(url));
        String[] keys = expression.split(Constants.JSON_EXPRESSION_SEPARATOR);
        JSONArray vehiculosJSON = null;
        for (String key : keys) {
            Object tmpJson = tmp.get(key);
            if (tmpJson instanceof JSONObject) {
                tmp = (JSONObject) tmpJson;
            } else if (tmpJson instanceof JSONArray) {
                vehiculosJSON = (JSONArray) tmpJson;
            }
        }

        return vehiculosJSON;
    }
}
