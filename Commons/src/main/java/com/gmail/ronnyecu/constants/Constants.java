package com.gmail.ronnyecu.constants;

public class Constants {
    public static String JSON_EXPRESSION_SEPARATOR = "\\.";
    public static String JSON_VEHICULO_EXPRESSION = "vehiculos" + "." + "vehiculo";
    public static String URL_CICLOMOTOR_JSON = "http://opendata.gijon.es/descargar.php?id=112&tipo=JSON";
    public static String URL_PERSONAS_JSON = "http://www.json-generator.com/api/json/get/cgxnkSunVK?indent=2";
}
